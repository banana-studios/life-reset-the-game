extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var speed = 400  # How fast the player will move (pixels/sec).
export var SneakSpeed = 150  # How fast the player will move (pixels/sec).
export var RunSpeed = 300  # How fast the player will move (pixels/sec).
signal isSneak
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity = Vector2()  # The player's movement vector.
	if Input.is_action_pressed("player_right"):
		velocity.x += 0.1
		$AnimatedSprite.play("WalkingHorizontal")
		$AnimatedSprite.flip_h = true
	if Input.is_action_pressed("player_left"):
		velocity.x -= 0.1
		$AnimatedSprite.play("WalkingHorizontal")
		$AnimatedSprite.flip_h = false
	if Input.is_action_pressed("player_down"):
		velocity.y += 0.1
		$AnimatedSprite.play("WalkingDown")
	if Input.is_action_pressed("player_up"):
		velocity.y -= 0.1
		$AnimatedSprite.play("WalkingUp")
	var curSpeed = speed
	if Input.is_action_pressed("player_run"):
		curSpeed = RunSpeed
		$AnimatedSprite.speed_scale = 1.5
	elif Input.is_action_pressed("player_sneak"):
		curSpeed = SneakSpeed
		$AnimatedSprite.speed_scale = 0.5
		emit_signal("isSneak")
	else:
		$AnimatedSprite.speed_scale = 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * curSpeed
	else:
		$AnimatedSprite.play("Default")
		$AnimatedSprite.stop()
	move_and_slide(velocity,Vector2(0, 0))
